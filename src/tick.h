#ifndef __GETTICK_H__
#define __GETTICK_H__

#if !defined(_WIN32)
unsigned long GetTickCount();
unsigned long GetTickCount2();
#endif /// of !defined(_WIN32)

#endif /// of __GETTICK_H__
