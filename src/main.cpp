#include <unistd.h>

#include <ctime>
#include <cmath>
#include <pthread.h>
#include <vector>
#include <string>
#ifndef NO_OMP
#include <omp.h>
#endif

#include <FL/x.H>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>

#include "fl_imgtk.h"
#include "tick.h"

//////////////////////////////////////////////////////////////////

using namespace std;

/////////////////////////////////////////////////////////////////

static Fl_Double_Window*    winMain = NULL;
static Fl_Box*              boxDraw = NULL;
static Fl_RGB_Image*        imgDraw = NULL;
static Fl_RGB_Image*        imgDrawBack = NULL;
static pthread_t            ptt;
static bool                 thread_killswitch = false;
static char                 strPerf[80] = {0};

/////////////////////////////////////////////////////////////////

unsigned LineMax = 1;
bool     HalfWindow = true;

// about sin wave ---- 
unsigned sw_step = 2;
float    sw_sz   = 0;
float    sw_hg   = 0;
float    sw_pd   = -1.7f;
float    sw_shx  = 0;
float    sw_shy  = 0;

typedef struct 
{
    int pos[4];
}LinePair;

typedef struct
{
    int x;
    int y;
}PointPair;

#define LINEPAIR    vector< LinePair >
#define POINTPAIR   vector< PointPair >

LINEPAIR    LineSrcs;
LINEPAIR    LineDsts;
POINTPAIR   SinWavePts;

/////////////////////////////////////////////////////////////////

/*
 *  hegt = in pixel, real height.
 */
void GenerateSinPoints( POINTPAIR &ppair, float hght, float shx, float shy, float period, int stp )
{
    unsigned szPts = ppair.size();

    if ( stp < 1 )
        stp = 1;

    float x = 0.f;
    float divf = ( 3.14f / (float)szPts ) * stp;

    for( unsigned cnt=0; cnt<szPts; cnt++ )
    {
        float xv = x * divf;
        int y = hght * sin( ( xv - shx ) / period ) + shy;
        x += (float)stp;

        ppair[ cnt ].x = x;
        ppair[ cnt ].y = y;
    }
}

void fl_wcb( Fl_Widget* w, void* p )
{
    if ( w == winMain )
    {
        thread_killswitch = true;
        usleep( 1000 );
        pthread_join( ptt, 0 );
        winMain->hide();
    }
}

bool CheckLinesMatch( LINEPAIR &a, LINEPAIR &b )
{
    if ( a.size() != b.size() )
        return false;

    for( unsigned cnt=0; cnt<a.size(); cnt++ )
    {
        for( unsigned xx=0; xx<4; xx++ )
        {
            if ( a[cnt].pos[xx] != b[cnt].pos[xx] )
                return false;
        }
    }   

    return true;
}

void GenerateNewLines( LINEPAIR &src )
{
    srand( unsigned( time( NULL ) ) );

    for( unsigned cnt=0; cnt<src.size(); cnt++ )
    {
        for( unsigned xx=0; xx<4; xx++ )
        {
            if ( cnt%2 == 0 )
            {
                src[cnt].pos[xx] = rand()%winMain->h();
            }
            else
            {
                src[cnt].pos[xx] = rand()%winMain->w();
            }
        }
    }
}

void MoveLines( LINEPAIR &src, LINEPAIR &dst )
{
    if ( src.size() < dst.size() )
        return;

    const int acc = 80;

    // check movements ...
    #pragma omp parallel for
    for( unsigned lp=0; lp<dst.size(); lp++ )
    {
       // X ...
        for( unsigned cnt=0; cnt<4; cnt+=2 )
        {
            int dist = 0;

            if ( dst[lp].pos[cnt] > src[lp].pos[cnt] )
            {
                dist = dst[lp].pos[cnt] - src[lp].pos[cnt];
            }
            else
            if ( dst[lp].pos[cnt] < src[lp].pos[cnt] )
            {
                dist = -( src[lp].pos[cnt] - dst[lp].pos[cnt] );
            }

            if ( dist != 0 )
            {
                int ndist = dist/acc;
                if ( ndist == 0 )
                {
                    ndist = dist;
                }

                src[lp].pos[cnt] += ndist;
            }
            else
            {
                src[lp].pos[cnt] = dst[lp].pos[cnt];
            }
        }
 
        // Y ...
        for( unsigned cnt=1; cnt<4; cnt+=2 )
        {
            int dist = 0;

            if ( dst[lp].pos[cnt] > src[lp].pos[cnt] )
            {
                dist = dst[lp].pos[cnt] - src[lp].pos[cnt];
            }
            else
            if ( dst[lp].pos[cnt] < src[lp].pos[cnt] )
            {
                dist = -( src[lp].pos[cnt] - dst[lp].pos[cnt] );
            }

            if ( dist != 0 )
            {
                int ndist = dist/acc;

                if ( ndist == 0 )
                {
                    ndist = dist;
                }

                src[lp].pos[cnt] += ndist;
            }
            else
            {
                src[lp].pos[cnt] = dst[lp].pos[cnt];
            }
        }
    }
}

void* threadRender( void* p )
{
    // Wait a sec for window shown.
    sleep( 1 );

    int  movetopos[4] = {0};
    int  curpos[4] = {0};
    bool repos = false;
    int  acc = 80;

    unsigned fpscheckt = GetTickCount();
    unsigned fpscheckv = 0;
    unsigned fpscheckf = 0;

    while( thread_killswitch == false )
    {
        // check positions...
        if ( CheckLinesMatch( LineSrcs, LineDsts ) == true )
        {
            GenerateNewLines( LineDsts );
        }

        MoveLines( LineSrcs, LineDsts );

        unsigned perf0 = GetTickCount();

        // -- don't need to lock,
        //Fl::lock();
        
        boxDraw->deimage();

        if ( imgDraw != NULL )
        {
            unsigned margin = imgDraw->w()/40;
            unsigned m_l = margin;
            unsigned m_t = margin;
            unsigned m_w = imgDraw->w() - ( margin * 2 );
            unsigned m_h = imgDraw->h() - ( margin * 2 );

            Fl_RGB_Image* imgTmpC = fl_imgtk::crop( imgDraw,
                                                    m_l, m_t, m_w, m_h );
            if ( imgTmpC != NULL )
            {
                imgDrawBack = fl_imgtk::rescale( imgTmpC,
                                                 imgDraw->w(),
                                                 imgDraw->h(),
                                                 fl_imgtk::BICUBIC );

                fl_imgtk::discard_user_rgb_image( imgTmpC );
            }
        }
        
        fl_imgtk::blurredimage_ex( imgDrawBack, 4 );
        fl_imgtk::brightness_ex( imgDrawBack, -2 );
        fl_imgtk::drawonimage( imgDraw, imgDrawBack, 0, 0, 0.4f );

        const unsigned col1 = 0xFF5555FF;
        const unsigned col2 = 0x5555FFFF;
        const float    ltwd = 2.1f;

        #pragma omp parallel for
        for ( unsigned cnt=0; cnt<LineSrcs.size(); cnt++ )
        {
            int flip_x1 = 0;
            int flip_y1 = 0;
            int flip_x2 = 0;
            int flip_y2 = 0;

            if ( cnt > 0 )
            {
                fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                               LineSrcs[cnt-1].pos[2],
                                               LineSrcs[cnt-1].pos[3],
                                               LineSrcs[cnt].pos[0],
                                               LineSrcs[cnt].pos[1],
                                               ltwd,
                                               col1 );

                flip_x1 = imgDraw->w() - LineSrcs[cnt-1].pos[2];
                flip_y1 = imgDraw->h() - LineSrcs[cnt-1].pos[3];
                flip_x2 = imgDraw->w() - LineSrcs[cnt].pos[0];
                flip_y2 = imgDraw->h() - LineSrcs[cnt].pos[1];

                fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                               flip_x1,
                                               flip_y1,
                                               flip_x2,
                                               flip_y2,
                                               ltwd,
                                               col2 );
                
            }

            fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                           LineSrcs[cnt].pos[0],
                                           LineSrcs[cnt].pos[1],
                                           LineSrcs[cnt].pos[2],
                                           LineSrcs[cnt].pos[3],
                                           ltwd,
                                           col1 );

            flip_x1 = imgDraw->w() - LineSrcs[cnt].pos[0];
            flip_y1 = imgDraw->h() - LineSrcs[cnt].pos[1];
            flip_x2 = imgDraw->w() - LineSrcs[cnt].pos[2];
            flip_y2 = imgDraw->h() - LineSrcs[cnt].pos[3];

            fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                           flip_x1,
                                           flip_y1,
                                           flip_x2,
                                           flip_y2,
                                           ltwd,
                                           col2 );
            
            if ( ( cnt + 1 ) == LineSrcs.size() )
            {
                fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                               LineSrcs[cnt].pos[2],
                                               LineSrcs[cnt].pos[3],
                                               LineSrcs[0].pos[0],
                                               LineSrcs[0].pos[1],
                                               ltwd,
                                               col1 );

                flip_x1 = imgDraw->w() - LineSrcs[cnt].pos[2];
                flip_y1 = imgDraw->h() - LineSrcs[cnt].pos[3];
                flip_x2 = imgDraw->w() - LineSrcs[0].pos[0];
                flip_y2 = imgDraw->h() - LineSrcs[0].pos[1];

                fl_imgtk::draw_smooth_line_ex( imgDraw, 
                                               flip_x1,
                                               flip_y1,
                                               flip_x2,
                                               flip_y2,
                                               ltwd,
                                               col2 );
            }
        }

        // draw sinGraph.
        #pragma omp parallel for
        for( unsigned cnt=1; cnt<SinWavePts.size(); cnt++ )
        {
            int x1 = SinWavePts[cnt-1].x;
            int y1 = SinWavePts[cnt-1].y;
            int x2 = SinWavePts[cnt].x;
            int y2 = SinWavePts[cnt].y;

            if ( x2 < x1 )
            {
                x2++;
            }
            else
            if ( x2 > x1 )
            {
                x2--;
            }

            if ( y2 < y1 )
            {
                y2++;
            }
            else
            if ( y2 > y1 )
            {
                y2--;
            }

            fl_imgtk::draw_smooth_line_ex( imgDraw,
                                           x1,
                                           y1,
                                           x2,
                                           y2,
                                           4.2f,
                                           0x66FF661F );
        }

        sw_shx -= sw_step / 50.f;

        if ( sw_shx < -(float)winMain->w() )
        {
            sw_shx = 0;
        }

        GenerateSinPoints( SinWavePts,
                           sw_hg,
                           sw_shx,
                           sw_shy,
                           sw_pd, 
                           sw_step );

        boxDraw->image( imgDraw );

        fl_imgtk::discard_user_rgb_image( imgDrawBack );

        unsigned perf1 = GetTickCount();

        fpscheckv += perf1 - perf0;
        fpscheckf ++;

        if ( fpscheckt < ( GetTickCount() - 1000 ) )
        {
            float avrperf = 1000.f / 
                            ( (float)fpscheckv / (float)fpscheckf );
       
            snprintf( strPerf, 80, 
                      "Multi thread drawing performance : %.2f fps ... ",
                      avrperf );

            boxDraw->label( strPerf );

            fpscheckt = GetTickCount();
            fpscheckv = 0;
            fpscheckf = 0;
        }

        // -- don't need to unlock.
        //Fl::unlock();
        boxDraw->redraw();
        winMain->redraw();

        Fl::awake();

#ifdef _WIN32
        InvalidateRect( fl_xid( winMain ), NULL, TRUE );
        Sleep( 0 );
#endif
    }

    pthread_exit( 0 );
    return NULL;
}

void parseArgs( int argc, char** argv )
{
    if ( argc > 1 )
    {
        for( unsigned cnt=1; cnt<argc; cnt++ )
        {
            string strTmp = argv[cnt];
    
            if ( strTmp.find( "--max:" ) != string::npos )
            {
                size_t fpos = strTmp.find_last_of( ":" );
                if ( fpos != string::npos )
                {
                    string strNum = strTmp.substr( fpos + 1 );
                    if ( strNum.size() > 0 )
                    {
                        int numTmp = atoi( strNum.c_str() );
                        if ( numTmp > 0 )
                        {
                            LineMax = numTmp;
                        }
                    }
                }
            }
            else
            if ( strTmp.find( "--fs" ) != string::npos )
            {
                HalfWindow = false;
            }
        }
    }
}

int main( int argc, char** argv )
{
    parseArgs( argc, argv );

    // Get RPI3 Screen size ...
    // Make current window left top place.
    int ms_x;
    int ms_y;
    Fl::get_mouse( ms_x, ms_y );

    int scrn_x;
    int scrn_y;
    int scrn_w;
    int scrn_h;
    Fl::screen_work_area( scrn_x, scrn_y, scrn_w, scrn_h,
                          Fl::screen_num( ms_x, ms_y ) );

    int new_w = ( scrn_w / 3 ) * 2;
    int new_h = ( scrn_h / 3 ) * 2;

    if ( HalfWindow == false )
    {
        new_w = scrn_w;
        new_h = scrn_h;
    }

    // Initialize fltk lock
    Fl::lock();

    snprintf( strPerf, 80, 
              "Multi-thread drawing performance checker, Raph.K." );

    winMain = new Fl_Double_Window( scrn_x, scrn_y, 
                                    new_w, new_h,
                                    strPerf );
    if ( winMain != NULL )
    {
        if ( HalfWindow == false )
        {
            winMain->border( 0 );
        }
        winMain->labelcolor( 0xFFFFFFFF );
        winMain->begin();

            boxDraw = new Fl_Box( 0,0,
                                  winMain->w(),
                                  winMain->h() );
            if ( boxDraw != NULL )
            {
                int flsz = winMain->h() / 40;
                if ( flsz < 14 )
                {
                    flsz = 14;
                }

                boxDraw->box( FL_FLAT_BOX );
                boxDraw->align( FL_ALIGN_CENTER |
                                FL_ALIGN_INSIDE |
                                FL_ALIGN_IMAGE_BACKDROP );
                boxDraw->labelfont( FL_COURIER );
                boxDraw->labelcolor( 0xFFFFFFFF );
                boxDraw->labelsize( flsz );
                boxDraw->color( 0 );

                imgDraw = \
                fl_imgtk::makeanempty( winMain->w(),
                                       winMain->h(),
                                       3, 
                                       0 );
                boxDraw->image( imgDraw );
            }

        winMain->callback( fl_wcb, winMain );
        winMain->end();
        winMain->show();

        if ( HalfWindow == false )
        {
            Fl::focus( winMain );
        }
    }

    // Wait for exposured.
    Fl::wait();

    // Generate Line Vectors.
    LineSrcs.resize( LineMax );
    LineDsts.resize( LineMax );
    GenerateNewLines( LineDsts );

    // Generate Sine wave form ...
    sw_sz   = winMain->w() / sw_step;
    sw_hg   = ( winMain->h() / 5 ) * 2.f;
    sw_shy  = winMain->h() / 2;

    SinWavePts.resize( sw_sz );
    GenerateSinPoints( SinWavePts,
                       sw_hg,
                       sw_shx,
                       sw_shy,
                       sw_pd, 
                       sw_step );

    // Makes pthread to drawing in thread.
    int tid = 0;

    if ( pthread_create( &ptt, NULL, threadRender, &tid ) == 0 )
    {
        // Elevate priority
        int ptpol;
        struct sched_param ptpar;
        pthread_getschedparam( ptt, &ptpol, &ptpar );
        ptpar.sched_priority = sched_get_priority_max( ptpol );
        pthread_setschedparam( ptt, ptpol, &ptpar );
    }

    int reti = Fl::run();

    // Destroy Line Vecotrs
    LINEPAIR().swap( LineSrcs );
    LINEPAIR().swap( LineDsts );
    POINTPAIR().swap( SinWavePts );

    fl_imgtk::discard_user_rgb_image( imgDrawBack );
    fl_imgtk::discard_user_rgb_image( imgDraw );

    return reti;
}
