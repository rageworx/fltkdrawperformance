# FLTK drawing performance checker for OpenMP.

A simple source code for FLTK drawing performance check in OpenMP ( Multi-thread ) driven.

## Requirements

* GCC, MinGW-W64
* FLTK-1.3.4-2-ts ( https://github.com/rageworx/fltk-1.3.4-2-ts )
* fl_imgtk ( https://github.com/rageworx/fl_imgtk )
* Make
* OpenMP supproted system.

## build

* Check all fl_imgtk library export files copy into lib directory.
* Just type make with your platform build types.
   - Makefile.linux : For Linux and Embedded Linux
   - Makefile.mingw : For MinGW-W64
   - Makefile.macosx : For MacOSX

## Supproted systems

* Linux, Embedded Linux ( Odroid UX4, Raspberry Pi 3 )
* Windows
* Mac OS X

## Executing parameters

* `--max:(line pairs)` : changes line pair counting ( default : 3 )
* `--fs` : full screen area of your desktop workspace.
